/**
 * Document design for dbSNP 2.0
 * @author Eric Moyer
 * @todo add copyright etc.
 *
 * The use of classes like Id and Uid etc is likely to carry on into
 * the main code. However, they will be concrete classes with value
 * semantics that carry no more overhead than the simple integers and
 * strings that they replace. In this document they function to
 * clarify the specification before I have written the JavaDoc. There,
 * they will aid in internal documentation but also provide minor aid
 * in avoiding errors through strong typing. This encapsulation also
 * centralizes the code dealing with them so that we can change to
 * using 128 bit integers rather than strings for Uids, for example
 * (this might have performance benefits in the future).
 *
 * Using uuids as the primary key in the vault (rather than straight
 * integers) is a good idea because you don't need to worry about
 * distribution when you generate a key. This will make a potential
 * transition to a widely distributed NoSQL vault much simpler. For
 * MSSQL, we want a different clustering key (to reduce fragmentation
 * - and thus help cache performance).
 *
 * My first thought was to use natural keys for the clustering key:
 *
 * - For RefSNP: ptlp, type, revision number (since refSNPs for nearby
 *   locations will be accessed together) I don't include organism
 *   with revision number because the ptlp includes a taxid
 *
 * - For SubSNP: ptlp, type, ssid, submitter, submission time (and we
 *   need to ensure that the same batchid cannot be submitted twice in
 *   the same second - and that ssid uniquely determines a batchid)
 *
 * - For Batch: organism, batchid, submitter, submission time (and we
 *   need to ensure that the same batchid cannot be submitted twice in
 *   the same second)
 *
 * However, [Kimberly Trip at the SQLSkills
 * website](http://www.sqlskills.com/blogs/kimberly/guids-as-primary-keys-andor-the-clustering-key/)
 * points out that wide clustering keys can waste a lot of space, so
 * we might just go with a rowid clustering key. This is a low-level
 * implementation decision that will depend on some "build it and see"
 * type of trials. 

 * For future reference when it becomes time to actually make this
 * decision, see 
 * [stack overflow](http://stackoverflow.com/questions/11938044/what-are-the-best-practices-for-using-a-guid-as-a-primary-key-specifically-rega),
 * [coding horror](http://blog.codinghorror.com/primary-keys-ids-versus-guids/),
 * and
 * [aspfaq.com](http://databases.aspfaq.com/database/what-should-i-choose-for-my-primary-key.html)
 *
 * @todo sync up descriptions from confluence
 */

import java.util.*;


/**
 * I am envisioning a super-command something like git or svn or mvn for the
 * first version of the console. So, one would type (for example)
 *
 * dbsnp status
 *
 * And it would print the status of each organism in dbsnp to the console
 */
class DbSNPConsole
{
    /**
     * Print status of all organisms
     */
    @SuppressWarnings("UnusedDeclaration")
    public static void Status()
    {
        g.for_each(Repository.global().organismIds(),
                new PrintOrganismStatus());
    }

    /**
     * Print status of the given organism
     *
     * @param id The tax-id of the organism to print
     */
    public static void Status(OrganismId id) {
        Optional<Organism> possOrg = Repository.global().organism(id);
        if (!possOrg.isPresent()) {
            System.out.println("Organism " + id.toString() + " is not in dbSNP.");
            return;
        }
        Organism o = possOrg.get();
        OrganismSnapshot latest = o.latestAsOf(Instant.now());
        DevelopingOrganismSnapshot snapshot = o.developingSnapshot();
        DevelopingOrganismSnapshotStage stage = snapshot.getStage();

        System.out.print(id.toString());
        System.out.print("\tMost recent build:\t"+latest.time());
        switch(stage)
        {
            case Normal:
                System.out.print("\tNot building");
                break;
            case Freezing:
                System.out.print("\tFreezing data"); //Might be hard to test
                                                     // this branch
                break;
            case Frozen:
                System.out.print("\tData frozen.\tFrozen at\t"+
                        snapshot.dataFreezeTime());
                assert snapshot.dataFreezeTime().isPresent();
                break;
            case Building:
                System.out.print("\tBuilding\tData frozen at\t"+snapshot
                        .dataFreezeTime()+"\tUsing RS as of\t"+snapshot
                        .rsFreezeTime()+"\tand current federated cache");
                assert snapshot.dataFreezeTime().isPresent();
                assert snapshot.rsFreezeTime().isPresent();
                break;
            case BuildFailed:
                System.out.print("\tBuild failed\tData frozen " +
                        "at\t"+snapshot
                        .dataFreezeTime()+"\tRS frozen at\t"+snapshot
                        .rsFreezeTime());
                assert snapshot.dataFreezeTime().isPresent();
                assert snapshot.rsFreezeTime().isPresent();
                break;
            case Validating:
                System.out.print("\tValidating\tData frozen at\t"+snapshot
                        .dataFreezeTime()+"\tRS frozen at\t"+snapshot
                        .rsFreezeTime());
                assert snapshot.dataFreezeTime().isPresent();
                assert snapshot.rsFreezeTime().isPresent();
                assert snapshot.products().isPresent();
                break;
            case StagingProducts:
                System.out.print("\tStaging Products\tData frozen at\t" +
                        snapshot.dataFreezeTime()+
                        "\tRS frozen at\t"+snapshot.rsFreezeTime());
                assert snapshot.dataFreezeTime().isPresent();
                assert snapshot.rsFreezeTime().isPresent();
                assert snapshot.products().isPresent();
                break;
            case StagingFailed:
                System.out.print("\tStaging Products Failed\tData frozen at\t" +
                        snapshot.dataFreezeTime()+
                        "\tRS frozen at\t"+snapshot.rsFreezeTime());
                assert snapshot.dataFreezeTime().isPresent();
                assert snapshot.rsFreezeTime().isPresent();
                assert snapshot.products().isPresent();
                break;
            case ReadyForRelease:
                System.out.print("\tReady for release\tData frozen at\t" +
                        snapshot.dataFreezeTime()+
                        "\tRS frozen at\t"+snapshot.rsFreezeTime());
                assert snapshot.dataFreezeTime().isPresent();
                assert snapshot.rsFreezeTime().isPresent();
                assert snapshot.products().isPresent();
                break;
            case xxDummy:
                System.out.print("\tDummy state - should never happen\tData " +
                        "frozen at\t" +
                        snapshot.dataFreezeTime()+
                        "\tRS frozen at\t"+snapshot.rsFreezeTime());
                assert false; // Should never happen
                break;
            default:
                System.out.print("\tUnknown state - should never happen\tData" +
                        " frozen at\t" + snapshot.dataFreezeTime()+
                        "\tRS frozen at\t" + snapshot.rsFreezeTime());
                assert false; // Should never happen
                break;
        }
        System.out.println();
    }

    public static void Advance(OrganismId id)
    {

    }

    public static void Retreat(OrganismId id)
    {

    }

    public static void SetBuildRsFreeze(OrganismId id, ReleaseNumber num)
    {

    }

    public static void ReleaseAllOrganisms()
    {

    }

    private static class PrintOrganismStatus implements Consumer<OrganismId> {
        @Override
        public void eat(OrganismId value) {
            DbSNPConsole.Status(value);
        }
    }
}

/**
 * The organism snapshot proceeds through stages starting at normal.
 *
 * On Release, all states transition to themselves unless otherwise
 * specified.
 *
 * The state transition diagram is:
 *
 * Normal:
 *     Advance -> Freezing
 *     Retreat -> Normal (or user error)
 *
 * Freezing:
 *     Can't advance or retreat until completes
 *     Success -> Frozen
 *     Failure -> Normal
 *
 * Frozen:
 *     Only in this state can the RS freeze time be set
 *     Advance -> Building
 *     Retreat -> Normal (RS freeze and Data freeze are wiped out)
 *
 * Building:
 *     Can't advance or retreat until completes
 *     Success -> Validating
 *     Failure -> Build Failed
 *
 * BuildFailed:
 *     Advance -> same as retreat
 *     Retreat -> Frozen (deletes any left-over products and debugging
 *         information)
 *
 * Validating:
 *     Advance -> Staging Products
 *     Retreat -> Frozen (deletes any left-over products)
 *
 * StagingProducts:
 *     Can't advance or retreat until completes
 *     Success -> ReadyForRelease
 *     Failure -> Staging Failed
 *
 * StagingFailed:
 *     Advance -> same as retreat
 *     Retreat -> Validating (undoes any staging and removes any debug
 *         information)
 *
 * ReadyForRelease:
 *     Advance -> ReadyForRelease (does nothing)
 *     Retreat -> Validating (undoes any staging)
 *     Release -> Normal (special)
 */
enum DevelopingOrganismSnapshotStage{
    Normal, Freezing, Frozen, Building, BuildFailed, Validating,
    StagingProducts, StagingFailed, ReadyForRelease, xxDummy
}

/** An instant in time
 *
 * I like using an instant in time to designate historical views
 * because it doesn't require much coordination beyond having decent
 * clocks in the machines doing the calculation. It also has very good
 * semantic clustering properties for use in an index (as opposed to
 * UIDs which are uncorrelated)
 *
 * I can search-and-replace this if I decide to use a synthetic
 * instant like release-number.
 *
 * It is probably best to use a DB-native date-time object to
 * represent an Instant. There will likely be internal parsing if we
 * need to see it directly. If we have to port to a new back-end, we
 * can just convert the Instants to what is supported there.
 */
class Instant
{
    public static Instant now(){ return A.<Instant>dummy().obj(); }
}

@SuppressWarnings("UnusedDeclaration")
interface HistoricalObject<Snapshot, IdT>
{
    /** Return the id of this object */
    public IdT id();

    /**
     * Return the snapshot that would have been the latest at {@code instant}
     *
     * Public-facing functions should only call this for dates that
     * have been released.
     *
     * @param instant only snapshots with a time no later than instant
     *     will be considered.
     */
    public Snapshot latestAsOf(Instant instant);

    /**
     * Allow iterating through all snapshots
     *
     * @todo Should this be available for major objects like Repository?
     */
    public Edible<Snapshot> all();
}

/**
 * A HistoricalObject viewed at a particular instant in time - the
 * time the snapshot was "taken"
 *
 * A snapshot's subject is the HistoricalObject of which it is a
 * view. This is analogous to a photographer's customer being the
 * subject of his photographs.
 */
@SuppressWarnings("UnusedDeclaration")
interface Snapshot<IdT>
{
    /** 
     * The instant at which the snapshot was "taken" 
     */
    public Instant time();

    /**
     * The id of the object of which the snapshot is a view
     */
    public IdT subjectId();
}

/** 
 * Uniquely identify the source of a supporting snp where its archival
 * representation is located. 
 *
 * Note that the xxDummy is a way of specifying that we are returning
 * a dummy value and should not be in the final implementation.
 */
@SuppressWarnings("UnusedDeclaration")
enum RsSupportSource {
    SubSnp, Genotype, EuroSubSnp, ClinVar,
        RefSnpIdRewriter, xxDummy}

/**
 * All things that can support a RefSnp implement RsSupport
 *
 * At this point, this includes SubSnps, ClinVar data and Genotype
 * data. These are also called "Supporting SNPs"
 */
@SuppressWarnings("UnusedDeclaration")
interface RsSupport
{
    /**
     * The sequence locations for the supporting record
     *
     * This will be exactly like observations(), projected onto the first
     * first component and eliminating duplicates
     */
    public Edible<SeqLoc> seqLocs();

    /**
     * The mapping from SeqLocs to observations for this variant
     *
     * If the record has observations of a variant and its reference for a
     * given SeqLoc, both are included in the returned Edible
     *
     * @return all sequence observations asserted by this variant
     */
    public Edible<Pair<SeqLoc, ShortSequence>> observations();


    /**
     * A routine to add associated data payload to a RefSnp
     *
     * @param rs the snapshot to receive the payload
     */
    public void addPayloadTo(RefSnpSnapshot rs);

    /**
     * A unique identifier for this supporting snp
     */
    public RsSupportId rsSupportId();
}


/**
 * The unique identifier of a biological sequence
 */
class SeqId extends StringId {}

/**
 * A variation
 */
class SimpleVariation
{
    /**
     * The location at which the variation is specified
     */
    public SeqLoc location(){ return A.<SeqLoc>dummy().obj(); }

    /**
     * The general type of the variation
     */
    public VariationInstType type(){ return VariationInstType.xxDummy; }

    /**
     * All the alleles represented by this variation
     *
     * (Multiple alleles are listed to take care of cases like M where
     * there is ambiguity. M would be represented as the list {A,C} - where
     * A and C are appropriate AlleleSpecifier objects.)
     */
    public Edible<AlleleSpecifier> alleles(){
        return new DummyEdible<AlleleSpecifier>(); }
}

/**
 *
 */
class RsSupportUtil
{
    /**
     * Return the types of the SNP after mapping to the target sequence
     *
     * @param supp The supporting SNP whose observations are being mapped
     * @param from Which SeqLoc is being mapped.
     *             Must be one of the SeqLocs supported by supp.
     * @param to Which sequence is it being mapped to
     * @param ads The ADS used to do the remapping
     * @return the variations represented by the {@code supp} object's
     *     observations at {@code from} when mapped to {@code to}
     */
    @SuppressWarnings("UnusedDeclaration")
    public static Edible<SimpleVariation>
    varType(RsSupport supp, SeqLoc from, SeqId to, ADS ads)
    {
        return new DummyEdible<SimpleVariation>();
    }

}

@SuppressWarnings("UnusedDeclaration")
class Id{}

class RsSupportId{
    /** 
     * An id that uniquely identifies this object among all objects
     * in the same source.
     *
     * For items whose normal id is an integer, this is the string
     * representation of that integer.
     *
     * This must be a string because some databases may use
     * string-based identifiers and they don't have a commonly
     * accepted numeric form whereas there is an obvious
     * transformation from integers to strings.
     *
     * If you concatenate this and the string representation of the
     * source, you get a unique identifier for the supporting snp
     * among all supporting snps.
     *
     * Such an identifier is needed to generate a report of which
     * supporting snps have a change in ptlp.
     *
     */
    public StringId internalId;

    /**
     * The archive that contains the canonical version of this
     * supporting information.
     */
    @SuppressWarnings("UnusedDeclaration")
    public RsSupportSource source;
}

/**
 * Causes the associated RS to move differently from the way the PTLP
 * would map.
 *
 * A RefSnpIdRewriter is a special type of supporting SNP generated
 * internally by NCBI to allow recording of manual tweaks performed in
 * our RefSNP mapping algorithm. In particular, a RefSnpIdRewriter
 * takes n locations and assigns different RS-ids to the RS objects at
 * those locations than would have been otherwise assigned.
 *
 * The RefSnpIdRewriter may be considered the prototype of RefSnp futzing
 * hacks. If we need to make any other manual tweaks to RefSNPs, they
 * can get their own supporting SNP object type.
 *
 * @todo This hasn't gotten a real going-over since November. It needs to be looked at in light of the changes
 */
@SuppressWarnings("UnusedDeclaration")
class RefSnpIdRewriter implements RsSupport {
    /** Represents a modification to a RefSnpId */
    interface IdModification
    {
        /** Return the Id to assign to a given oldId 
         *
         * @param oldId the Id that would exist without any modification
         *
         * @return the Id to assign to oldId
         */
        public RefSnpId newId(RefSnpId oldId);
    }
    /** Represents allocating a new RefSnpId value */
    static class AllocateId implements IdModification
    {
        @Override
        public RefSnpId newId(RefSnpId oldId){ 
            // Allocate a new RefSnpId
            return A.<RefSnpId>dummy().obj(); 
        }
    }
    /** Represents setting the RsId to a particular value */
    static class SetId implements IdModification{
        final RefSnpId toSet_;
        SetId(RefSnpId toSet){ toSet_ = toSet; }
        @Override
        public RefSnpId newId(RefSnpId oldId)
        {
            return toSet_;
        }
    }
    /** Represents not modifying the Id */
    static class Unmodified implements IdModification{
        @Override
        public RefSnpId newId(RefSnpId oldId)
        {
            return oldId;
        }
    }

    /**
     * @todo implement
     */
    @Override
    public Edible<SeqLoc> seqLocs(){ return new DummyEdible<SeqLoc>(); }

    /**
     * @todo implement
     */
    @Override
    public Edible<Pair<SeqLoc, ShortSequence>> observations()
    { return new DummyEdible<Pair<SeqLoc, ShortSequence>>(); }

    /**
     * A routine to add associated data payload to a RefSnp
     *
     * @todo implement
     */
    @Override
    public void addPayloadTo(RefSnpSnapshot rs){}

    /**
     * A unique identifier for this supporting snp
     *
     * @todo implement
     */
    @Override
    public RsSupportId rsSupportId(){return A.<RsSupportId>dummy().obj();}
    
}

@SuppressWarnings("UnusedDeclaration")
class IntegerId{
    public Long value;
}
@SuppressWarnings("UnusedDeclaration")
class StringId{
    public String value;
}

@SuppressWarnings("UnusedDeclaration")
class Allele{}
@SuppressWarnings("UnusedDeclaration")
class AlleleSpecifier
{
    public RefSnpId id(){ return A.<RefSnpId>dummy().obj(); }
    public ReleaseNumber build(){ return A.<ReleaseNumber>dummy().obj(); }
    public Allele allele(){ return A.<Allele>dummy().obj(); }
}


@SuppressWarnings("UnusedDeclaration")
enum CensorshipState { Normal, Suppressed, Withdrawn, xxDummy }
@SuppressWarnings("UnusedDeclaration")
class CensorshipMetadata
{
    String byWhom(){ return A.<String>dummy().obj(); }
    String why(){ return A.<String>dummy().obj(); }
    Date when(){ return A.<Date>dummy().obj(); }
}

@SuppressWarnings({"UnusedDeclaration","SameReturnValue"})
class CensorshipOrder
{
    Edible<SubSnpId> toCensor(){ return new DummyEdible<SubSnpId>(); }
    CensorshipState targetState() { return CensorshipState.xxDummy; }
    CensorshipMetadata metadata() { return A.<CensorshipMetadata>dummy().obj(); }
}


@SuppressWarnings("UnusedDeclaration")
class SubSnpId extends IntegerId{}
@SuppressWarnings("UnusedDeclaration")
class SubSnp implements HistoricalObject<SubSnpSnapshot, SubSnpId>
{
    @Override
    public SubSnpId id(){ return A.<SubSnpId>dummy().obj(); }
    @Override
    public SubSnpSnapshot latestAsOf(Instant instant){
        return A.<SubSnpSnapshot>dummy().obj();
    }
    @Override
    public Edible<SubSnpSnapshot> all(){
        return new DummyEdible<SubSnpSnapshot>(); 
    }
}
@SuppressWarnings("UnusedDeclaration")
class SubSnpSnapshot implements Snapshot<SubSnpId>, RsSupport
{
    @Override
    public Instant time(){ return A.<Instant>dummy().obj(); }
    @Override
    public SubSnpId subjectId(){ return A.<SubSnpId>dummy().obj(); }

    public OrganismId organismId(){ return OrganismId.Dummy(); }
    @SuppressWarnings({"SameReturnValue"})
    public CensorshipState censorshipState() { return CensorshipState.xxDummy; }
    public Optional<CensorshipMetadata> lastCensorship() {
        return Optional.Dummy(); }

    @Override
    public Edible<SeqLoc> seqLocs(){ return new DummyEdible<SeqLoc>(); }

    @Override
    public Edible<Pair<SeqLoc, ShortSequence>> observations()
    { return new DummyEdible<Pair<SeqLoc, ShortSequence>>(); }

    @Override
    public void addPayloadTo(RefSnpSnapshot rs)
    { 
        rs.addSubSnpId(subjectId()); 
    }

    @Override
    public RsSupportId rsSupportId()
    { 
        RsSupportId id = new RsSupportId();
        id.internalId = new StringId();
        id.internalId.value = subjectId().value.toString();
        id.source = RsSupportSource.SubSnp;
        return id;
    }
}

@SuppressWarnings("UnusedDeclaration")
class ClinVarSnp implements RsSupport
{
    @Override
    public Edible<SeqLoc> seqLocs(){ return new DummyEdible<SeqLoc>(); }

    @Override
    public Edible<Pair<SeqLoc, ShortSequence>> observations()
    { return new DummyEdible<Pair<SeqLoc, ShortSequence>>(); }

    @Override
    public void addPayloadTo(RefSnpSnapshot rs){ /* todo: implement */ }
    @Override
    public RsSupportId rsSupportId()
    { 
        RsSupportId id = new RsSupportId();
        id.internalId = new StringId();
        id.source = RsSupportSource.ClinVar;
        return id;
    }
}

@SuppressWarnings("UnusedDeclaration")
class GenotypeSnp implements RsSupport
{
    @Override
    public Edible<SeqLoc> seqLocs(){ return new DummyEdible<SeqLoc>(); }

    @Override
    public Edible<Pair<SeqLoc, ShortSequence>> observations()
    { return new DummyEdible<Pair<SeqLoc, ShortSequence>>(); }

    @Override
    public void addPayloadTo(RefSnpSnapshot rs){ /* todo: implement */ }
    @Override
    public RsSupportId rsSupportId()
    { 
        RsSupportId id = new RsSupportId();
        id.internalId = new StringId();
        id.source = RsSupportSource.Genotype;
        return id;
    }
}

/** 
 * Just a stand-in for properly designing the annotations to match the
 * ASN.1 object.
 */
@SuppressWarnings("UnusedDeclaration")
class Annotation {}

@SuppressWarnings("UnusedDeclaration")
class RefSnpId extends IntegerId{}
/** Note: RefSnpCore objects are not separate. The core is just an
 * annotated RefSnp with an empty list of annotations. */
@SuppressWarnings("UnusedDeclaration")
class RefSnp implements HistoricalObject<RefSnpSnapshot, RefSnpId>
{
    @Override
    public RefSnpId id(){ return A.<RefSnpId>dummy().obj(); }
    @Override
    public RefSnpSnapshot latestAsOf(Instant instant){
        return A.<RefSnpSnapshot>dummy().obj();
    }
    @Override
    public Edible<RefSnpSnapshot> all(){
        return new DummyEdible<RefSnpSnapshot>(); 
    }
}
@SuppressWarnings({"UnusedParameters","UnusedDeclaration"})
class RefSnpSnapshot implements Snapshot<RefSnpId>
{
    public RefSnpSnapshot(){}
    public RefSnpSnapshot(RefSnpSnapshot rss){}

    /** 
     * Return this RSS or a new one depending on whether the ptlp
     * would have to change
     *
     * If the ptlp is the same as this snapshot, return this
     * one. Otherwise, make a copy with the new PTLP and return that.
     *
     * @return this RSS or a copy with different ptlp depending on
     *     whether the ptlp would have to change
     */
    public RefSnpSnapshot withPtlp(PTLP newPtlp){
        if(newPtlp == ptlp()){
            return this;
        }else{
            RefSnpSnapshot rs = new RefSnpSnapshot(this);
            rs.setPtlp(newPtlp);
            return rs;
        }
    }

    @Override
    public Instant time(){ return A.<Instant>dummy().obj(); }
    @Override
    public RefSnpId subjectId(){ return A.<RefSnpId>dummy().obj(); }

    public OrganismId organismId(){ return OrganismId.Dummy(); }
    public PTLP ptlp(){ return A.<PTLP>dummy().obj(); }
    @SuppressWarnings("EmptyMethod")
    private void setPtlp(PTLP newPtlp){}

    public RsAnchor anchor(){ return RsAnchor.Dummy(); }
    public Edible<Annotation> annotations() {
        return new DummyEdible<Annotation>(); }

    public Edible<SubSnpId> subSnpIds() {
        return new DummyEdible<SubSnpId>(); }
    /** Adds the given id to the list of supporting ids */
    @SuppressWarnings("EmptyMethod")
    void addSubSnpId(SubSnpId support){}
}

@SuppressWarnings("UnusedDeclaration")
class BatchId extends IntegerId{}
@SuppressWarnings("UnusedDeclaration")
class Batch implements HistoricalObject<BatchSnapshot, BatchId>
{
    @Override
    public BatchId id(){ return A.<BatchId>dummy().obj(); }
    @Override
    public BatchSnapshot latestAsOf(Instant instant){
        return A.<BatchSnapshot>dummy().obj();
    }
    @Override
    public Edible<BatchSnapshot> all(){
        return new DummyEdible<BatchSnapshot>(); 
    }
}
@SuppressWarnings("UnusedDeclaration")
class BatchSnapshot implements Snapshot<BatchId>
{
    @Override
    public Instant time(){ return A.<Instant>dummy().obj(); }
    @Override
    public BatchId subjectId(){ return A.<BatchId>dummy().obj(); }

    public OrganismId organismId(){ return OrganismId.Dummy(); }
    public Edible<SubSnpId> subSnpIds() { return new DummyEdible<SubSnpId>(); }
}

/**
 * A non-public snapshot - of which there is at most one per organism
 *
 * One thing I need to consider is creating the new support RADI from
 * a parent RADI + modifications. When the vault is implemented as a
 * MSSQL database, scanning the vault is bad - hard to do in
 * parallel. However, because of panfs, we can access "disk" storage
 * in parallel efficiently.
 *
 * Using the old RADI provides a cache of the vault in a
 * parallel-accessible format. Moreover, for most items (whose PTLPs
 * have not changed) the old bin is the bin that it needs to go in, so
 * a big bulk insert will put the majority of snps into the right
 * bin. The remaining items will require their own transactions to
 * insert. New supporting SNPs are inserted into their own clean RADI
 * as they arrive. Then after the old RADI has been updated, the new
 * SNPs are also remapped and inserted into the appropriate
 * places. This, however, requires duplicating the SNP information,
 * storing one copy in the Vault and one in one of two
 * RADIs. Additionally, the RADI must be kept around in order to
 * build. Also, if you want to change the build in some way,
 * (different data freeze etc) you end up messing with the RADI and
 * extracting data for different dates or going back and rescanning
 * the Vault. You also end up needing to collapse SNP updates in both
 * the Vault and in the RADI. Further, more parts of the system will
 * depend on the RADI's implementation because it is now used not just
 * for direct building, but also as a cache storing new SNPs. This
 * constrains the possibilities for optimization in the future and
 * makes the RADI interface more rigid. Finally, this gives up on the
 * Consistency (or Atomicity depending on how you look at it)
 * properties of the SNP store. Something can be added to the Vault,
 * but not yet be in the cached RADI (or vice versa). This is, however
 * a penalty of moving things outside a single database. The RADI will
 * need a way of moving items before a certain date to another RADI to
 * allow for a proper data freeze.
 *
 * An alternative is to make the Vault more easily scannable by
 * removing it from MS SQL Server. For example, you could use the same
 * structure as the RADI except instead of using the hash of the
 * anchor, you use the hash of the id to divide the database into
 * equal-sized buckets (or you could use the id itself and just have
 * the buckets dynamically split when they get too big). The range
 * query for different versions shouldn't produce many results, so can
 * be handled in the leaves. Building this way is slightly less
 * efficient because each bin of the Vault will have destinations
 * evenly distributed over the entire RADI. However, by sorting before
 * doing the writes, we can avoid too much slowdown. This keeps one
 * master-copy of everything, so that atomicity is preserved. It also
 * simplifies the conceptual design - the RADI becomes a temporary
 * cache structure used for building. On the other hand, the Vault
 * will depend more heavily on our code and may be more vulnerable to
 * corruption (though I don't think this likely since SQLite will
 * still do the heavy lifting for concurrency locking and transaction
 * rollbacks).
 *
 * Making a parallel-accessible Vault is very tempting because this is
 * not the first time that the "we can't access the vault efficiently"
 * problem has come up. Since we expect that we will need to replace
 * MSSQL server in the future, we can avoid that cost.
 *
 * I have decided that a parallel-vault is crucial. We have two basic
 * work-loads, key-value and map-reduce. The key-value can be done by
 * any key-value store. However, the ones that support map-reduce also
 * allow parallel key-value access.
 * 
 * A discussion of potential storage technologies on 21 Nov 2014
 * verified my thought of trying to implement this in the public cloud
 * using map-reduce and brought out the importance of having a stable,
 * dependable append-only store locally for recovery from disaster. We
 * might also use Amazon Glacier. Either system will need periodic
 * checks to make sure we can restore from it.
 */
class DevelopingOrganismSnapshot{
    /**
     * The stage in this organism's development of the snapshot
     */
    private DevelopingOrganismSnapshotStage stage_;
    /**
     * Access to this organism's products (if they have been created)
     */
    private Optional<Edible<Product>> products_;

    @SuppressWarnings("WeakerAccess")
    public OrganismId id(){ return OrganismId.Dummy(); }

    /** The time for the last data freeze 
     *
     * Absent when no data freeze time has been set
     */
    private Optional<Instant> dataFreezeTime_;

    /** The time for the rs freeze - the frozen rs are in remappedRs
     *
     * Absent when no data freeze time has been set
     */
    @SuppressWarnings("UnusedDeclaration")
    private Optional<Instant> rsFreezeTime_;

    /** The ads used for producing remappedRs_ */
    @SuppressWarnings("CanBeFinal")
    private ADS ads_;

    /** Cache of the remapped RS */
    @SuppressWarnings("CanBeFinal")
    private RADI<RefSnpSnapshot> remappedRs_;

    /** Cache of the remapped supporting data */
    @SuppressWarnings("CanBeFinal")
    private RADI<RsSupport> remappedSupport_;

    /** 
     * Holds the report of the RS remapping operations that produced
     * remappedRs_
     *
     * Absent when no remapping has been done
     */
    private Optional<RsRemapReport> rsRemapReport_;

    /** 
     * Holds the report of the Supporting SNP remapping operations
     * that produced remappedSupport_
     *
     * Absent when no remapping has been done
     */
    private Optional<SupportRemapReport> supportRemapReport_;

    /** Snapshot using the given ads */
    @SuppressWarnings("UnusedDeclaration")
    DevelopingOrganismSnapshot(ADS ads)
    {
        ads_ = ads;
        dataFreezeTime_ = new Optional<Instant>();
        rsFreezeTime_ = new Optional<Instant>();

        rsRemapReport_ = new Optional<RsRemapReport>();
        remappedRs_ = RADI.Empty();

        supportRemapReport_ = new Optional<SupportRemapReport>();
        remappedSupport_ = RADI.Empty();

        stage_ = DevelopingOrganismSnapshotStage.Normal;
        products_ = new Optional<Edible<Product>>();
    }

    /** Set the data freeze for this snapshot to now. */
    @SuppressWarnings("UnusedDeclaration")
    public void freezeData()
    { 
        dataFreezeTime_ = 
            new Optional<Instant>(Instant.now()); 
        // The remapped support depends on the data freeze time, so
        // when that changes, the remapped support also changes. The
        // simple way is just to get rid of it and calculate
        // again. The complicated (but possibly faster) way is to only
        // delete those items that are newer than the new freeze time
        // and then remap all items that fall between the old and new
        // times. Below is the simple way.
        remappedSupport_.deleteAll();
    }

    @SuppressWarnings("WeakerAccess")
    public Optional<Instant> dataFreezeTime()
    { return dataFreezeTime_; }

    /**
     * Set the remapped rs cache to the remapping of all rs before the
     * rs freeze
     *
     * Specifying a different batch data freeze time from the rs data
     * freeze time allows the rs data to be earlier, which is useful
     * in the case where a software regression has resulted in bad
     * PTLPs in a particular build (or another problem with the build)
     * that got past our QC and went public and we want to base our
     * (emergency fix) on the previous build, but not lose the snps
     * submitted since then.
     *
     * This can be called more than once. This allows remapping to
     * occur between builds. However, if updates to the ADS are
     * released or there is a need to ignore the RS used to do the
     * previous remapping (for example, bad locations were assigned to
     * public RS and we need to use the correct locations from a
     * previous revision) the remapping can be done after the data freeze.
     *
     * @param rsDataFreezeTime The most recent snapshot for each
     *     RefSnp before rsDataFreeze will be chosen for remapping.
     */
    @SuppressWarnings("UnusedDeclaration")
    public void remapRsAsOf(Instant rsDataFreezeTime)
    {
        rsFreezeTime_ = new Optional<Instant>(rsDataFreezeTime);

        remappedRs_.deleteAll();
        rsRemapReport_ = new Optional<RsRemapReport>(new RsRemapReport());

        
        Edible<RefSnpSnapshot> rs = Repository.global().organism(id()).get()
            .latestRsAsOf(rsDataFreezeTime);
        remappedRs_.add(g.filter(rs, new RsRemapper(ads_, 
                                                    rsRemapReport_.get())));
    }

    /** 
     * Collect all the federated supporting snps and put them in the RADI
     *
     * Remapping is not the right term for this, but I can't think of a
     * better one right now.
     *
     * Possibly this should be part of freezeData 
     *
     * I did not put it there because freezeData is supposed to be an
     * instant operation and this operation will not be instant
     *
     *
     */
    @SuppressWarnings("UnusedDeclaration")
    public void remapSupport()
    {
        remappedSupport_.deleteAll();
        supportRemapReport_ = new Optional<SupportRemapReport>
            (new SupportRemapReport());

        // Combine all the supporting snps into one edible
        Organism org = Repository.global().organism(id()).get();
        Instant last = dataFreezeTime().get();
        Edible<RsSupport> ss = g.<RsSupport>cast(org.latestSsAsOf(last));
        Edible<RsSupport> cv = g.<RsSupport>cast(org.latestClinVarAsOf(last));
        Edible<RsSupport> gt = g.<RsSupport>cast(org.latestGenotypeAsOf(last));
        Edible<RsSupport> supp = g.concat(ss, g.concat(cv, gt));

        // Calculate their PTLPs and put them in the RADI
        remappedSupport_.add
            (g.flatten(g.filter(supp,
                    new SupportRemapper(ads_, supportRemapReport_.get()))));
    }

    /**
     * Free up all temporary resources (like disk space for remapped
     * RS) used in developing the new organism snapshot
     *
     * This would be a destructor in C++.
     */
    public void deleteSelf() { remappedRs_.deleteAll(); }


    /**
     * Return the stage for this organism in its building process
     * @return the stage for this organism in its building process
     */
    public DevelopingOrganismSnapshotStage getStage() {
        return stage_;
    }

    public Optional<Instant> rsFreezeTime() {
        return rsFreezeTime_;
    }

    public Optional<Edible<Product>> products() {
        return products_;
    }


    @SuppressWarnings("UnusedDeclaration")
    static class RsRemapper
        implements Transformer<RefSnpSnapshot, RADIEnvelope<RefSnpSnapshot>>{
        private final ADS ads_;
        private final RsRemapReport report_;
        public RsRemapper(ADS ads, RsRemapReport report){
            ads_ = ads; report_ = report; }
        @Override
        public RADIEnvelope<RefSnpSnapshot> transform(RefSnpSnapshot rs){
            // Do something with the remap report here
            RefSnpSnapshot remap = rs.withPtlp(ads_.ptlpFor(rs.ptlp()));
            return new RADIEnvelope<RefSnpSnapshot>(remap.anchor(), remap);
        }
    }

    static class SeqLocToPTLP
        implements Transformer<SeqLoc, Pair<SeqLoc,PTLP>>{
        private final ADS ads_;
        public SeqLocToPTLP(ADS ads){ ads_ = ads; }
        @Override
        public Pair<SeqLoc, PTLP> transform(SeqLoc seqLoc){
            return new Pair<SeqLoc, PTLP>(seqLoc, ads_.ptlpFor(seqLoc));
        }
    }

    static class VariationWhenReferenceIs
        implements Transformer<Pair<SeqLoc, PTLP>, Edible<Pair<PTLP, SimpleVariation>>>{

        private final ADS ads_;
        private final RsSupport supp_;

        VariationWhenReferenceIs(ADS ads, RsSupport supp)
        { ads_ = ads; supp_ = supp; }

        @Override
        public Edible<Pair<PTLP, SimpleVariation>> transform(Pair<SeqLoc, PTLP> orig)
        {
            return g.filter(
                    RsSupportUtil.varType(supp_, orig.getKey(),
                            orig.getValue().seqId(), ads_),
                    new AddPtlp(orig.getValue()));
        }

        private static class AddPtlp implements Transformer<SimpleVariation,
                Pair<PTLP, SimpleVariation>>
        {
            private final PTLP ptlp_;
            public AddPtlp(PTLP ptlp)
            {
                ptlp_ = ptlp;
            }

            @Override
            public Pair<PTLP, SimpleVariation> transform(SimpleVariation var)
            {
                return new Pair<PTLP, SimpleVariation>(ptlp_,var);
            }
        }
    }

    private static class WrapInEnvelope
            implements Transformer<Pair<PTLP,SimpleVariation>,
            RADIEnvelope<RsSupport>>
    {
        private final RsSupport supp_;
        public WrapInEnvelope(RsSupport supp)
        { supp_ = supp; }

        @Override
        public RADIEnvelope<RsSupport> transform(Pair<PTLP, SimpleVariation>
                                                         value)
        {
            return new RADIEnvelope<RsSupport>(
                    new RsAnchor(value.getKey(),value.getValue().type()),supp_);
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    static class SupportRemapper
        implements Transformer<RsSupport, Edible<RADIEnvelope<RsSupport>>>{
        private final ADS ads_;
        private final SupportRemapReport report_;
        public SupportRemapper(ADS ads, SupportRemapReport report){
            ads_ = ads; report_ = report; }
        @Override
        public Edible<RADIEnvelope<RsSupport>> transform(RsSupport rs){
            // Do something with the remap report here
            Edible<Pair<PTLP, SimpleVariation>> variations =
                    g.flatten(g.filter(g.filter(rs.seqLocs(), new SeqLocToPTLP(ads_)),
                    new VariationWhenReferenceIs(ads_, rs)));
            return g.filter(variations, new WrapInEnvelope(rs));
        }
    }

}

class NoDevelopingSnapshot extends Exception
{ @SuppressWarnings("UnusedDeclaration")
static final long serialVersionUID = 1L; }

@SuppressWarnings("UnusedDeclaration")
class OrganismId extends IntegerId
{
    @SuppressWarnings("FieldCanBeLocal")
    private final long taxId_;
    public OrganismId(long taxId){
        this.taxId_ = taxId;
    }
    public static OrganismId Dummy(){ return new OrganismId((new java.util.Random()).nextInt()); }
}
@SuppressWarnings("UnusedDeclaration")
class Organism implements HistoricalObject<OrganismSnapshot, OrganismId>
{
    private DevelopingOrganismSnapshot developing_;

    @Override
    public OrganismId id(){ return OrganismId.Dummy(); }
    @Override
    public OrganismSnapshot latestAsOf(Instant instant){
        return new OrganismSnapshot(new Instant(), new Instant());
    }
    @Override
    public Edible<OrganismSnapshot> all(){
        return new DummyEdible<OrganismSnapshot>(); 
    }

    public Edible<RefSnpSnapshot> latestRsAsOf(Instant time){
        return new DummyEdible<RefSnpSnapshot>();
    }
    public Edible<SubSnpSnapshot> latestSsAsOf(Instant time){
        return new DummyEdible<SubSnpSnapshot>();
    }
    public Edible<GenotypeSnp> latestGenotypeAsOf(Instant time){
        return new DummyEdible<GenotypeSnp>();
    }
    public Edible<ClinVarSnp> latestClinVarAsOf(Instant time){
        return new DummyEdible<ClinVarSnp>();
    }

    @SuppressWarnings({"SameReturnValue"})
    public boolean canPublishDevelopingSnapshot(){ return false; }
    @SuppressWarnings({"EmptyMethod", "RedundantThrows"})
    public void publishDevelopingSnapshot() throws NoDevelopingSnapshot {}

    /** 
     * Dispose of any existing developing snapshot and start a new one.
     *
     * The new snapshot uses the current time as the data freeze
     * point.
     *
     * @return the new snapshot
     */
    public DevelopingOrganismSnapshot startNewSnapshot(ADS ads)
    {
        if(developing_ != null){
            developing_.deleteSelf();
        }
        developing_ = new DevelopingOrganismSnapshot(ads);
        return developing_;
    }

    /**
     * Return this organism's developing snapshot
     * @return this organism's developing snapshot
     *
     * @todo Make sure that developing_ can never be null
     */
    public DevelopingOrganismSnapshot developingSnapshot() {
        return developing_;
    }
}
@SuppressWarnings("UnusedDeclaration")
class OrganismSnapshot implements Snapshot<OrganismId>
{
    /** The time at which the data-freeze took place for this snapshot */
    @SuppressWarnings("CanBeFinal")
    private Instant dataFreezeInstant_;

    /** RS snapshots used in this build must have a time at or before
     *  rsFreezeInstant */
    @SuppressWarnings({"CanBeFinal", "FieldCanBeLocal"})
    private Instant rsFreezeInstant_;

    public OrganismSnapshot(Instant dataFreezeInstant,
                            Instant rsFreezeInstant){
        dataFreezeInstant_ = dataFreezeInstant;
        rsFreezeInstant_ = rsFreezeInstant;
    }

    /**
     * The data freeze time for this snapshot
     * @return the data freeze time for this snapshot
     */
    @Override
    public Instant time(){ return dataFreezeInstant_; }
    @Override
    public OrganismId subjectId(){ return OrganismId.Dummy(); }

    public OrganismId organismId(){ return OrganismId.Dummy(); }
}

/** Provides an ID class for the Repository
 * 
 * I was originally thinking that there would be only one repository,
 * however, we need at least a QA repository and a Production
 * repository.
 */
@SuppressWarnings("UnusedDeclaration")
class RepositoryId extends StringId{}

/**
 * For ease-of-use the repository snapshots have their own id, the
 * release-number
 */
@SuppressWarnings("UnusedDeclaration")
class ReleaseNumber extends IntegerId{}

@SuppressWarnings("UnusedDeclaration")
class Repository implements HistoricalObject<RepositorySnapshot, RepositoryId>
{
    @SuppressWarnings("CanBeFinal")
    private Vault vault_=A.<Vault>dummy().obj();
    private Map<ReleaseNumber, Instant> release_times_;
    @SuppressWarnings("CanBeFinal")
    private static Repository global_repo_ = A.<Repository>dummy().obj();

    /** @todo what does the vault do in the history-object world? */
    public Vault vault(){ return vault_; }
    public static Repository global(){ return global_repo_; }

    @Override
    public RepositoryId id(){ return A.<RepositoryId>dummy().obj(); }

    @Override
    public RepositorySnapshot latestAsOf(Instant instant){
        return A.<RepositorySnapshot>dummy().obj();
    }

    @Override
    public Edible<RepositorySnapshot> all(){
        return new DummyEdible<RepositorySnapshot>(); 
    }

    public Edible<OrganismId> organismIds() {
        return new DummyEdible<OrganismId>(); }

    public Optional<Organism> organism(OrganismId id){ return Optional.Dummy();}

    /** @todo unreleased organisms may have updates after a single build time so we need the release time per organism so that we can only get snps released before that organism's last build */
    public Optional<Instant> releaseTime(ReleaseNumber bn){
        if(release_times_.containsKey(bn)){
            return new Optional<Instant>(release_times_.get(bn));
        }
        return new Optional<Instant>();
    }

    public void addRelease(ReleaseNumber bn, Instant in,
                           Collection<OrganismId> organisms){
        if(release_times_.containsKey(bn)){
            throw new RE("Build number "+bn.toString()+" already exists!");
        }
        publiclyDeployAllProductsInUnreleasedOrganisms();
        release_times_.put(bn, in);
    }



    /** @todo do I need to access the unreleased organism snapshot - probably, but I'm not sure */
    // public UnreleasedRevision getLatestUnReleased(Organism o){ 
    //     return A.<DummyRevision>dummy().obj(); 
    // }


    static class CensoredBatchCreator implements Transformer<BatchAndContents, 
        BatchSnapshot>{
        private final CensorshipOrder order_;
        public CensoredBatchCreator(CensorshipOrder o){order_ = o; }
        //Note that the snapshot will contain its creation time
        @Override
        public BatchSnapshot transform(BatchAndContents bc){
            return A.<BatchSnapshot>dummy().obj();
        }
    }
    static class BatchAndContents extends Pair<BatchId, Edible<SubSnpId> >{
        static final long serialVersionUID = 1L;
        BatchId batchId;
        Edible<SubSnpId> contents;
    }
    public Edible<BatchAndContents> batchesContaining(Edible<SubSnpId> subSnps){
        return new DummyEdible<BatchAndContents>();
    }
    public void censorSubSnps(CensorshipOrder order)
    {
        vault().batches().add
            (g.filter
             (batchesContaining(order.toCensor()),
              new CensoredBatchCreator(order) ));
    }

    /** Return a block of numToAllocate unused refSnp ids 
     *
     * After this method finishes the ids returned will not be usable
     * by any other call in the future - whether they ever appear in
     * the database or not.
     *
     * @param numToAllocate the number of refSnps in the returned Jen
     *
     * @return refSNPIds available for use by the caller
     *
     * @throws NotEnoughIdsException if there are not enough ids
     *     available to allocate.
     */
    @SuppressWarnings({"RedundantThrows", "UnusedDeclaration"})
    public Edible<RefSnpId> allocateRefSnpIds(int numToAllocate)
        throws NotEnoughIdsException
    { return new DummyEdible<RefSnpId>(); }

    public AlleleSpecifier convertToBuild(AlleleSpecifier spec, ReleaseNumber b)
    { return A.<AlleleSpecifier>dummy().obj(); }

    /** @todo the (currently commented out) code needs updating to new structure of HistoryObjects and snapshots */
    public AlleleSpecifier convertToLatest(AlleleSpecifier spec)
    {
        /*
        RefSnp rs = get(spec.id());
        ReleasedRevision rel_rev = getLatestReleased(rs.organism());
        return convertToBuild(spec, rel_rev.releaseNumber());
        */
        return A.<AlleleSpecifier>dummy().obj();
    }

    @SuppressWarnings({"EmptyMethod"})
    private void publiclyDeployAllProductsInUnreleasedOrganisms(){}
}

/** Each snapshot corresponds to one release of the repository */
@SuppressWarnings("UnusedDeclaration")
class RepositorySnapshot implements Snapshot<RepositoryId>
{
    @Override
    public Instant time(){ return A.<Instant>dummy().obj(); }
    @Override
    public RepositoryId subjectId(){ return A.<RepositoryId>dummy().obj(); }

    public ReleaseNumber id(){ return A.<ReleaseNumber>dummy().obj(); }

    public Organism get(OrganismId o){ return A.<Organism>dummy().obj(); }
    public RefSnp get(RefSnpId id){ return A.<RefSnp>dummy().obj(); }
    public SubSnp get(SubSnpId id){ return A.<SubSnp>dummy().obj(); }
    public RepositorySnapshot get(ReleaseNumber b){
        return A.<RepositorySnapshot>dummy().obj(); }
}


@SuppressWarnings("UnusedDeclaration")
interface Predicate<T>{
    public boolean test(T t);
}

@SuppressWarnings("UnusedDeclaration")
interface Consumer<T>
{
    public void eat(T value);
}
@SuppressWarnings("UnusedDeclaration")
interface Transformer<T,U>
{
    public U transform(T value);
}

@SuppressWarnings("UnusedDeclaration")
interface Edible<T>
{
    /** If there is a front element - consume it with c and return
     *  true; otherwise, return false.
     *
     * @param c The consumer that will eat the first element if
     *     present
     */
    public boolean eatFront(Consumer<T> c);
}

@SuppressWarnings("UnusedDeclaration")
class SingleEdible<T> implements Edible<T>
{
    private T val_;
    private boolean will_eat_;
    public SingleEdible(T t){ val_ = t;  will_eat_ = true; }
    @Override
    public boolean eatFront(Consumer<T> c)
    {
        boolean success = will_eat_;
        if(will_eat_){
            will_eat_ = false;
            c.eat(val_);
            val_ = null;
        }
        return success;
    }
}
@SuppressWarnings("UnusedDeclaration")
class DummyEdible<T> implements Edible<T>
{
    @Override
    public boolean eatFront(Consumer<T> c){ return false; }
}



/**
 * Global methods
 */
@SuppressWarnings({"UnusedParameters","UnusedDeclaration"})
class g{
    public static <T> Edible<T> select_if(Predicate<T> p, Edible<T> fromList){
        return new DummyEdible<T>(); }
    public static <T,U> Edible<U> filter(Edible<T> fromList,
                                         Transformer<T,U> transformer){
        return new DummyEdible<U>(); }
    public static <T> Edible<T> flatten(Edible<Edible<T>> fromList){
        return new DummyEdible<T>(); }
    public static <T,U> Edible<U> for_each(Edible<T> fromList,
                                           Consumer<T> consumer){ 
        return new DummyEdible<U>(); }
    public static <T> Edible<T> concat(Edible<T> a, Edible<T> b){
        return new DummyEdible<T>(); }

    public static <T> Edible<T> cast(Edible<? extends T> toCast){
        return new DummyEdible<T>();
    }
}

@SuppressWarnings("UnusedDeclaration")
class NotEnoughIdsException extends Exception{
    static final long serialVersionUID = 1L;
}




@SuppressWarnings("UnusedDeclaration")
enum ProductId{
    Web, Entrez, NADB, VCF_FTP, VarLoc
}

@SuppressWarnings("UnusedDeclaration")
enum ServiceId{
}

@SuppressWarnings("UnusedDeclaration")
interface RevisionProgressListener
{
    void transitionComplete(Revision r, ProcessStatus status);
}

@SuppressWarnings("UnusedDeclaration")
class RevisionNumber extends Id{}
@SuppressWarnings("UnusedDeclaration")
class RevisionId extends Id{}

/** @todo replace with another abstraction for all the snps and batches that need to be modified from a particular revision - this will likely be the result of a greatest-n-per-group query */
@SuppressWarnings("UnusedDeclaration")
class UidDump
{
    /*    Dump<RefSnpId> ref;
    Dump<SubSnpId> sub;
    Dump<BatchId> batch; */
}

@SuppressWarnings("UnusedDeclaration")
interface Revision
{
    /** Uniquely identifies a revision among all revisions */
    public RevisionId id();
    public OrganismId organism();

    /** Gives the place in the sequence of revisions for this organism
     *
     * The second revision produced for this organism (whether it was
     * ever made public or not) gets a revision number of 2.
     */
    public RevisionNumber revisionNumber();

    public Edible<Batch> getBatch(BatchId id);
    @SuppressWarnings({"SameReturnValue"})
    public boolean isReleased();

    @SuppressWarnings({"EmptyMethod"})
    public void nextStage(RevisionProgressListener l);

    public UidDump dumpUids();

    public ADS ads();
}

@SuppressWarnings({"EmptyMethod", "UnusedDeclaration"})
interface UnreleasedRevision extends Revision
{
    public void withdrawSubSnps(CensorshipOrder order);
    public void suppressSubSnps(CensorshipOrder order);
    public void addOrUpdateBatch(Batch b);
}

@SuppressWarnings({"UnusedDeclaration","SameReturnValue"})
interface FrozenRevision extends Revision
{
    public boolean hasProduct(ProductId id);
    public boolean providesService(ServiceId id);
    public Product getProduct(ProductId id);
    public Service getService(ServiceId id);
}

@SuppressWarnings("UnusedDeclaration")
interface ReleasedRevision extends Revision
{
    public ReleaseNumber releaseNumber();
}

/**
 * One of the biological entities making up a sequence (biological polymer)
 *
 * A nucleotide or amino-acid
 */
@SuppressWarnings("UnusedDeclaration")
class Monomer{}

/**
 * A short nucleotide or protein sequence
 *
 * There is a location 0 which is special.
 */
@SuppressWarnings("UnusedDeclaration")
class ShortSequence
{
    Edible<Monomer> all(){ return new DummyEdible<Monomer>(); }
    Monomer at(int offset){ return A.<Monomer>dummy().obj(); }
}

/**
 * The metadata that we use from a SeqFeat
 */
@SuppressWarnings("UnusedDeclaration")
class SeqFeatMetadata{}

/**
 * The delta field from a Variation-inst
 *
 * Models how the given sequence changes in the specific variation
 */
@SuppressWarnings("UnusedDeclaration")
class VariationDelta
{
    /**
     * Return the result of this delta occurring at the given sequence location
     * 0 <p/>
     *
     * @param before The sequence where the delta occurs. Must have at least
     *               neighborhoodNeeded() monomers around the SeqLoc. before will
     *               not be modified.
     *
     * @return The neighborhood after the change.
     */
    public ShortSequence applyTo(ShortSequence before){
        return A.<ShortSequence>dummy().obj();}

    /**
     * Return the delta needed to transform before into after
     *
     * @param before The sequence before the operation is applied
     * @param after The sequence after the operation is applied
     * @return An operation to transform before into after
     */
    public static VariationDelta diff(ShortSequence before, ShortSequence after){
        return A.<VariationDelta>dummy().obj();}


    /**
     * Return the size of the neighborhood needed to apply this delta
     * @return the size of the neighborhood needed to apply this delta
     */
    public int neighborhoodNeeded(){return dummyNeighborhoodSize; }
    private static final int dummyNeighborhoodSize = 100;
}

/**
 * Anchor+Delta+Metadata
 */
@SuppressWarnings("UnusedDeclaration")
class SeqFeat
{
    /** The location for this SeqFeat */
    SeqLoc loc(){ return A.<SeqLoc>dummy().obj(); }
    VariationInstType type(){ return A.<VariationInstType>dummy().obj(); }
    SeqFeatMetadata metadata() { return A.<SeqFeatMetadata>dummy().obj(); }
    VariationDelta delta() { return A.<VariationDelta>dummy().obj(); }

    /**
     * Return true if the reference allele was also observed, false if only the
     * variant allele was observed at this location.
     *
     * @return true if the reference allele was also observed, false if only the
     * variant allele was observed at this location.
     */
    boolean referenceWasObserved() { return dummyBoolean; }
    private static final boolean dummyBoolean = false;

    /**
     * Create a SeqFeat from its components
     * @param loc the location
     * @param type the type
     * @param delta the delta
     * @param md the metadata
     * @return A SeqFeat with the given location, delta, and metadata
     */
    public static SeqFeat fromComponents(SeqLoc loc, VariationInstType type,
                                         VariationDelta delta,
                                         SeqFeatMetadata md)
    {
        return A.<SeqFeat>dummy().obj();
    }
}

/** Alignment Data Set */
@SuppressWarnings("UnusedDeclaration")
class ADS
{
    private Optional<PTLP> ptlpFromLocalData(SeqLoc loc){
        return Optional.Dummy(); }
    private PTLP ptlpFromWebService(SeqLoc loc){ return A.<PTLP>dummy().obj(); }
    public PTLP ptlpFor(SeqLoc loc)
    {
        Optional<PTLP> local = ptlpFromLocalData(loc);
        if(local.isPresent()){ return local.get(); }
        return ptlpFromWebService(loc);
    }

    /** No clue how this will work */
    public SeqFeatMetadata remap(SeqFeatMetadata oldMd, ShortSequence before,
                                 ShortSequence after, SeqLoc oldLoc,
                                 SeqLoc newLoc){
        return A.<SeqFeatMetadata>dummy().obj();
    }

    /**
     * Return the location(s) corresponding to old in the latest reference
     * sequence release
     *
     * @param old a SeqLoc on (potentially) an old version.
     * @return the location(s) corresponding to old
     */
    public SeqLoc latest(SeqLoc old){ return A.<SeqLoc>dummy().obj(); }

    public Edible<SeqFeat> remap(SeqFeat feat){
        return remap(feat, latest(feat.loc()));
    }

    /** Chose how the new delta will be interpreted given that it used to be
     *  oldType
     *
     * @param oldType The type of variation before remapping
     * @param newDelta The actual change being enacted by the variation
     * @return The type assigned to the delta taking into account its old type
     *   to try to clear up some ambiguity.
     */
    public VariationInstType remap(VariationInstType oldType,
                                   VariationDelta newDelta){
        // This method will likely be complex and ugly and could require more
        // parameters to do the disambiguation properly
        return VariationInstType.xxDummy;
    }

    /**
     *
     * @param feat
     * @param newLoc
     * @return
     *
     * @todo finish implementing AND how do I know whether the reference was already remapped and has its own SeqFeat? See the fix comment.
     */
    public Edible<SeqFeat> remap(SeqFeat feat, SeqLoc newLoc)
    {
        VariationDelta delta = feat.delta();

        SeqLoc oldLoc = feat.loc();
        ShortSequence oldRef = oldLoc.getNeighborhood(
                delta.neighborhoodNeeded());

        ShortSequence newRef = newLoc.getNeighborhood(
                delta.neighborhoodNeeded());

        ShortSequence observed = delta.applyTo(oldRef);
        VariationDelta newDelta = VariationDelta.diff(newRef, observed);
        VariationInstType newType = remap(feat.type(), newDelta);

        SeqFeatMetadata newMd = remap(feat.metadata(), newRef, observed,
                oldLoc, newLoc);

        ArrayList<SeqFeat> remapped = new ArrayList<SeqFeat>(2);
        remapped.add(SeqFeat.fromComponents(newLoc, newType, newDelta, newMd));

        if(! oldRef.equals(newRef) && feat.referenceWasObserved()) {
            VariationDelta refDelta = VariationDelta.diff(newRef, oldRef);
            SeqFeatMetadata refMd = remap(feat.metadata(), newRef, oldRef,
                    newLoc, oldLoc);
            VariationInstType refType = VariationInstType.xxDummy; //Fix remapping the type - there is no previous type
        }


        return new DummyEdible<SeqFeat>();
    }
}

/** 
 * Report detailing changes that came out of remapping the previous
 * revision's contents to a new ADS 
 */
@SuppressWarnings("UnusedDeclaration")
class RsRemapReport implements Product
{
    @Override
    public ProductStage stage(){ return ProductStage.xxDummy; }

    @Override
    public void nextStage(ProductStageListener stageCompletionHandler){}

    @Override
    public void delete(ProductStageListener stageCompletionHandler){}
}

/** 
 * Report detailing changes that came out of remapping the previous
 * revision's supporting SNPs to a new ADS.
 *
 * This really will need a constructor taking the support assignments
 * before remapping so there can be a comparison.
 */
@SuppressWarnings("UnusedDeclaration")
class SupportRemapReport implements Product
{
    @Override
    public ProductStage stage(){ return ProductStage.xxDummy; }

    @Override
    public void nextStage(ProductStageListener stageCompletionHandler){}

    @Override
    public void delete(ProductStageListener stageCompletionHandler){}
}

@SuppressWarnings("UnusedDeclaration")
class Store<Obj, ObjId>
{
    @SuppressWarnings({"WeakerAccess","UnusedDeclaration"})
    static class Uid<ObjId>{
        ObjId id;
        Instant time;
    }

    public Edible<Obj> get(Edible<ObjId> ids, Instant asOf)
    {return new DummyEdible<Obj>();}

    @SuppressWarnings({"EmptyMethod"})
    public void add(Edible<Obj> toAdd){}

    @SuppressWarnings({"EmptyMethod"})
    public void deleteAll(){}
}

/* Comment out InitializingRevision so things keep compiling
class TemporaryRefSnpStore extends Store<RefSnp, RefSnpId>{}
class TemporarySubSnpStore extends Store<SubSnp, SubSnpId>{}
class TemporaryBatchStore extends Store<Batch, BatchId>{}
class InitializingRevision implements UnreleasedRevision
{
    private ADS ads_;
    private Organism organism_;
    private RevisionId id_;
    private RevisionNumber revisionNumber_;
    private Collection<BatchUid> batches_;
    private Collection<RefSnpUid> refSnps_;
    private Collection<SubSnpUid> subSnps_;
    private TemporaryRefSnpStore refSnpsChangedInRemapping_;
    private TemporaryBatchStore censorshipBatches_;
    private TemporarySubSnpStore censorshipSubSnps_;
    private RemapReport remapReport_;

    class RevisionHasBatchUid implements Predicate<Batch> {
        public boolean test(Batch b){
            return batches_.contains(b.uid());
        }
    }

    private void addAllFrom(UidDump d){}
*/
    /** Remap the RefSnps, SubSnps and Batches according to ads_
     *
     * Any new objects created will be placed in
     * refSnpsChangedInRemapping_. dump will be altered so its uids
     * contain the latest ones after remapping.
     *
     * @note for the implementation, I think new refSNPs will not have
     *     refSnpIds until the very end at which time, a bulk request
     *     for new ids will be made to the repository and those ids
     *     assigned to the objects.
     *
     * @param dump the uids of the objects to remap. Any objects that
     *     change in the remapping will have their uids replaced with
     *     new ones whose corresponding objects are stored in
     *     refSnpsChangedInRemapping_
     *
     * @param originalAds the original alignment data set used by dump
     *     at the moment it is passed in.
     */
    /*    private RemapReport remap(UidDump dump, ADS originalAds){ return remapReport_;}

    private RemapReport sameAdsRemapReport(){ return A.<RemapReport>dummy().obj(); }

    public InitializingRevision(FrozenRevision parent, ADS ads){
        ads_ = ads;
        organism_ = parent.organism();
        id_ = Repository.global().nextRevisionId();
        
        revisionNumber_ = parent.revisionNumber(); 
        ++revisionNumber_.value;

        UidDump dump = parent.dumpUids();
        if(!ads_.equals(parent.ads())){
            remapReport_ = remap(dump, parent.ads());
        }else{
            remapReport_ = sameAdsRemapReport();
        }
        addAllFrom(dump);
    }


    public ADS ads(){ return ads_; }
    public RevisionId id(){ return id_; }
    public Organism organism(){ return organism_; }
    public RevisionNumber revisionNumber(){ return revisionNumber_; }

    public RemapReport remapReport(){ return remapReport_; }
    public TemporaryRefSnpStore refSnpsChangedInRemapping(){
        return refSnpsChangedInRemapping_;}

    public Edible<Batch> getBatch(BatchId id)
    { 
        SingleEdible<BatchId> ide = new SingleEdible<BatchId>(id);
        return g.select_if(new RevisionHasBatchUid(), 
                           Repository.global().vault().batches().byId(ide));
    }

    public boolean isReleased(){ return false; }

    public void suppressSubSnps(CensorshipOrder order){}
    public void withdrawSubSnps(CensorshipOrder order){}

    public void addOrUpdateBatch(Batch b){}

    public UidDump dumpUids(){ return A.<UidDump>dummy().obj(); }

    public void nextStage(RevisionProgressListener l){}

}
    */

/** Just here so things keep compiling. Makes no semantic sense. */
@SuppressWarnings("UnusedDeclaration")
class DummyRevision implements UnreleasedRevision, ReleasedRevision, FrozenRevision{
    @Override
    public ADS ads(){ return A.<ADS>dummy().obj(); }
    @Override
    public RevisionId id() { return A.<RevisionId>dummy().obj(); }
    @Override
    public OrganismId organism() { return OrganismId.Dummy(); }
    @Override
    public RevisionNumber revisionNumber(){ return A.<RevisionNumber>dummy().obj(); }
    @Override
    public boolean hasProduct(ProductId id){ return false; }
    @Override
    public boolean providesService(ServiceId id){ return false; }
    @Override
    public Product getProduct(ProductId id){ throw new RE(); }
    @Override
    public Service getService(ServiceId id){ throw new RE(); }
    @Override
    public Edible<Batch> getBatch(BatchId id){ return new DummyEdible<Batch>();}
    @Override
    public void addOrUpdateBatch(Batch b){ }
    @Override
    public void suppressSubSnps(CensorshipOrder order){}
    @Override
    public void withdrawSubSnps(CensorshipOrder order){}
    @Override
    public boolean isReleased(){ return true; } //Arbitrarily call the dummy released
    @Override
    public ReleaseNumber releaseNumber(){ return A.<ReleaseNumber>dummy().obj(); }
    @Override
    public void nextStage(RevisionProgressListener l){}
    @Override
    public UidDump dumpUids(){ return A.<UidDump>dummy().obj(); }
}

@SuppressWarnings("UnusedDeclaration")
enum VariationInstType{ Snv, Other, xxDummy} //There's lots more but this will do for now


@SuppressWarnings("UnusedDeclaration")
class SeqLoc
{
    /**
     * Return the given neighborhood around this location
     * <p/>
     * For example if in the sequence A,A,C,C,G,G,T,T, (with the first location
     * being 0) a SeqLoc specified 2-5 (so C,C,G,G). getNeighborhood(1) would
     * return A,C,C,G,G,T with location 0 at the first C.
     *
     * @param size The number of Monomers upstream and downstream to return
     *
     * @return size monomers upstream and downstream from this SeqLoc's
     *     boundaries. Location 0 will be at this SeqLoc's lower bound
     */
    public ShortSequence getNeighborhood(long size){
        return A.<ShortSequence>dummy().obj(); }

    /**
     * The SeqId of the sequence on which this seq-loc is defined
     */
    public SeqId seqId()
    {
        return A.<SeqId>dummy().obj();
    }
}
@SuppressWarnings("UnusedDeclaration")
class PTLP extends SeqLoc {}

/** RsAnchor is my new name to replace Individuation. Simple search
 * and replace if I need to change it (and change the name of RADI:
 * "Rs Anchor Disk Index")  */
@SuppressWarnings("UnusedDeclaration")
class RsAnchor
{
    public RsAnchor(PTLP ptlp, VariationInstType type){}
    public PTLP ptlp(){ return A.<PTLP>dummy().obj(); }
    @SuppressWarnings({"SameReturnValue"})
    public VariationInstType type(){ return VariationInstType.xxDummy; }
    public static RsAnchor Dummy() { 
        return new RsAnchor(new PTLP(), VariationInstType.Snv); }
}





@SuppressWarnings("UnusedDeclaration")
class Dump<T>
{
    @SuppressWarnings({"EmptyMethod"})
    public void process(Consumer< Edible<T> > action)
    {
        // Split the dump into tasks then process them in parallel
    }
}

@SuppressWarnings("UnusedDeclaration")
class RADIEnvelope<Snp>
{
    private final RsAnchor anchor_;
    private final Snp snp_;
    public RADIEnvelope(RsAnchor anchor, Snp snp)
    {
        anchor_ = anchor;
        snp_ = snp;
    }

    public RsAnchor anchor() { return anchor_; }
    public Snp snp(){ return snp_; }

    /** Utility method to allow my dummy code to return a dummy
     * envelope - not part of the design */
    public static <S> RADIEnvelope<S> Dummy(){
        RsAnchor ra = RsAnchor.Dummy();
        S snp = A.<S>dummy().obj();
        return new RADIEnvelope<S>(ra, snp); 
    }
}

@SuppressWarnings("UnusedDeclaration")
class RADIHandle<Snp>{}

/** RADI is "Rs Anchor Disk Index" */
@SuppressWarnings("UnusedDeclaration")
class RADI<Snp>
{
    /** Get all the RsAnchor keys in the map and put them in the dump */
    public Dump<RsAnchor> dumpKeys(){ return new Dump<RsAnchor>(); }

    /** Create an empty RADI
     *
     */
    public static <S> RADI<S> Empty() {
        return new RADI<S>(); }

    public static <S> RADI<S> FromHandle(RADIHandle<S> h) {
        return new RADI<S>(); }

    @SuppressWarnings({"EmptyMethod"})
    public void add(Edible<RADIEnvelope<Snp>> toAdd){}

    public RADIEnvelope<Snp> at(RsAnchor c){ return RADIEnvelope.Dummy(); }

    /** Remove all SNPs stored in this RADI */
    @SuppressWarnings({"EmptyMethod"})
    public void deleteAll(){}
}

@SuppressWarnings("UnusedDeclaration")
class Release
{
    public ReleaseNumber releaseNumber(){return A.<ReleaseNumber>dummy().obj(); }
    public Edible<RevisionId> revisions(){
        return new DummyEdible<RevisionId>(); }
}

@SuppressWarnings("UnusedDeclaration")
enum ProcessStatus
{
    Success, Failure
}

@SuppressWarnings("UnusedDeclaration")
interface ProductStageListener
{
    void transitionComplete(Product p, ProductStage curStage,
                            ProcessStatus status);
}

@SuppressWarnings("UnusedDeclaration")
enum ProductStage
{
    PreExistence, Created, MostlyDeployed, PubliclyDeployed, Destroyed,
        xxDummy
}
@SuppressWarnings({"SameReturnValue", "UnusedDeclaration","EmptyMethod"})
interface Product
{
    public ProductStage stage();

    /** Advance to the next stage, informing stageCompletionHandler 
     *
     * The next stage after Destroyed is PreExistence. This allows
     * starting over by calling delete and then nextStage twice.
     *
     * @todo check if there are products for which this "starting
     *     over" wouldn't make sense. If so, make calling nextStage in
     *     the Destroyed state an error and create two subtypes of the
     *     main product interface, one with a "StartOver" method that
     *     can only be called in the destroyed stage.
     */
    public void nextStage(ProductStageListener stageCompletionHandler);

    /** Go directly to Destroyed stage */
    public void delete(ProductStageListener stageCompletionHandler);
    
}

@SuppressWarnings("UnusedDeclaration")
class Service{}

/**
 * Cold storage is high reliability backup for the things we archive.
 *
 * It could be implemented locally as an MSSQL database (or better, a
 * column store like VDB which can compress it mightily) or in the
 * cloud using a service like Glacier. It needs to be write once, read
 * seldom. Another possibility Douglas mentioned is a massive text
 * file on disk (though there will have to be some way of interleaving
 * concurrent updates) - maybe a massive SQLite database on disk?
 */
@SuppressWarnings({"UnusedDeclaration"})
class ColdStorage
{
    @SuppressWarnings({"UnusedDeclaration","WeakerAccess"})
    static class ThawedObject{
        /** The time the object was created */
        Instant creationTime;

        /** 
         * The time the object was stored 
         *
         * This can be used to implement a post-facto overwriting on
         * retrieval by looking at which version of the object was
         * stored most recently.
         */
        Instant storageTime;

        /**
         * A string identifying the format used to serialize the
         * serialized object. It 
         */
        String serializationFormat;
        
        /**
         * A serialized version of the object
         */
        String serializedObject;
    }

    /**
     * Add an object to cold storage
     *
     * @param id An id for this object among all its peers with the
     *    same object type. Retrieval is by (id,type) pair. There is
     *    no such thing as overwriting. Though the retriever can
     *    opt to only look at the most-recent stored version.
     *
     * @param objectType a string identifying the type of object
     *   stored in serializedObject. Should include some version
     *   information because it and the format must be sufficient for
     *   deserialization.
     *
     * @param creation the instant the serializedObject was created
     *
     * @param serializationFormat a string uniquely identifying the
     *   format in which the object was serialized. This should
     *   include some version information because it plus the type
     *   must be enough to deserialize the object for future clients.
     *
     * @param serializedObject an object of type objectType serialized
     *   in format serializationFormat
     */
    @SuppressWarnings({"EmptyMethod"})
    public void put(StringId id, String objectType,
                    Instant creation, String serializationFormat, 
                    String serializedObject){}
    /**
     * Retrieve all versions of an object from cold storage
     *
     * @param id An id for this object among all its peers with the
     *    same object type.
     *
     * @param objectType a string identifying the type of object
     *   stored in serializedObject. Should include some version
     *   information because it and the format must be sufficient for
     *   deserialization.
     */
    public Edible<ThawedObject> get(StringId id, String objectType){
        return new DummyEdible<ThawedObject>();
    }
}

/** @todo write this up */
@SuppressWarnings("UnusedDeclaration")
class HotStorage{
    
}

@SuppressWarnings("UnusedDeclaration")
class Vault
{

    public Store<RefSnpSnapshot, RefSnpId> refSnps() {
        return new Store<RefSnpSnapshot, RefSnpId>();
    }
    public Store<SubSnpSnapshot, SubSnpId> subSnps() {
        return new Store<SubSnpSnapshot, SubSnpId>();
    }
    public Store<BatchSnapshot, BatchId> batches(){
        return new Store<BatchSnapshot, BatchId>();
    }
    public Store<OrganismSnapshot, OrganismId> organisms(){
        return new Store<OrganismSnapshot, OrganismId>(); 
    }
    /** There will really only be one repository whose history is
     * stored, but it still makes sense to store and update its
     * history using the same interface as the rest */
    public Store<RepositorySnapshot, RepositoryId> repositories(){
        return new Store<RepositorySnapshot, RepositoryId>(); 
    }

    /** Add Batches and the associated SubSnps to batches_ and subSnps_ */
    @SuppressWarnings({"EmptyMethod", "UnusedDeclaration"})
    public void addBatches(Edible<Batch> batches){}

}

/** Short name for RuntimeException */
@SuppressWarnings("UnusedDeclaration")
class RE extends RuntimeException
{
    static final long serialVersionUID = 1L;
    RE(){}
    RE(String s){}
}

/** Work-around for Java Generics limitation
 *
 * We can't create instances of type parameters. This fixes that.
 *
 * To use: <code>Make.&lt;MyTypeParam&gt;right().now()</code>
 */
@SuppressWarnings("UnusedDeclaration")
class Make<T>
{
    /** Class of T
     *
     * @warning would need to be initialized in real code - any real use
     *    of this will result in a NullPointerException
     */
    private Class<T> tClass;
    public static <U> Make<U> right(){ return new Make<U>(); }
    public T now()
    { 
        try{ return tClass.newInstance(); 
        }catch(InstantiationException ex){ return null;
        }catch(IllegalAccessException ex){ return null;
        }
    }
}

/**
 * Helper for creating dummy objects
 *
 * Exactly the same as the Make class but marks the creation as a
 * dummy object.
 *
 * To use: <code>A.&lt;MyTypeParam&gt;dummy().obj()</code>
 */
@SuppressWarnings("UnusedDeclaration")
class A<T>
{
    /** Class of T
     *
     * @warning would need to be initialized in real code - any real use
     *    of this will result in a NullPointerException
     */
    private Class<T> tClass;
    public static <U> A<U> dummy(){ return new A<U>(); }
    public T obj()
    { 
        try{ return tClass.newInstance(); 
        }catch(InstantiationException ex){ return null;
        }catch(IllegalAccessException ex){ return null;
        }
    }
}

@SuppressWarnings("UnusedDeclaration")
class Optional<T>{
    private T value_;
    private boolean is_present_;
    public Optional(){ is_present_ = false; }
    public Optional(T value){
        if(value == null){
            throw new NullPointerException(""); 
        }
        value_ = value;
    }
    public boolean isPresent() { return is_present_; }
    public T get() throws NoSuchElementException {
        if(isPresent()){ 
            return value_; 
        }else{ 
            throw new NoSuchElementException("Optional value not present.");
        }
    }
    public void ifPresent(Consumer<? super T> consumer){
        if(isPresent()){ consumer.eat(value_); }
    }

    public static <U> Optional<U> Dummy(){ return new Optional<U>(); }
}

@SuppressWarnings("UnusedDeclaration")
class Pair<K,V> extends AbstractMap.SimpleImmutableEntry<K,V>{
    static final long serialVersionUID = 1L;
    Pair(){ super(null, null); }
    Pair(K key, V value){
        super(key,value);
    }
}
