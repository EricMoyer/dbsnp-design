class SSGrowOperation:
    def SetUp(handle):
        IndividuationSNPMap<SubSNP>.AllowWriters(handle)
        snp_pdi = IndividuationSNPMapMutable<SubSNP>(handle)

    # In reality, this needs to handle removals too
    def ReceivedSubmission(submission_dump_location):
        try:
            snp_pdi.AddFrom(submission_dump_location)
        except sanity_check:
            notify_maintainers_of_bad_stuff()

    def Done:
        IndividuationSNPMap<SubSNP>.DisallowWriters(snp_pdi.handle)

class SSMergeOperation:

    def Setup(new_ss_handle, init_ss_handle, blank_ss_handle):
        IndividuationSNPMap<SubSNP>.DisallowWriters(new_ss_handle)
        IndividuationSNPMap<SubSNP>.DisallowWriters(init_ss_handle)
        IndividuationSNPMap<SubSNP>.AllowWriters(blank_ss_handle)

        merge_ss_db = IndividuationSNPMapMutable<SubSNP>(blank_ss_handle)
        new_ss_db = IndividuationSNPMap<SubSNP>(new_ss_handle)
        init_ss_db = IndividuationSNPMap<SubSNP>(init_ss_handle)
    
    def AddSSToDict(dest, ss_list):
        for ss in ss_list:
            dest[ss.ss_id] = ss


    # The individuation generator must iterate over the union of the
    # individuations in the new_ss and merge_ss databases
    def Process(individuation_generator):
        for individuation in individuation_generator:
            merged = dict()
            AddSSToDict(merged, init_ss_pdi.At(individuation))
            for ss in new_ss_pdi.At(individuation):
                ident = ss.ss_id
                if exists(merged[ident]):
                    old = merged[ident]
                    if ss.is_add:
                        # Can't add new when already in existence. A
                        # move (= delete+add) would have the add at a
                        # different individuation. If the ss was not
                        # live, the SSID cannot be reused, so this is
                        # still an error.
                        error()
                    elif ss.is_delete:
                        # All live and non-live ss-es can be deleted;
                        # deletion is always part of a move. Purge is
                        # not implemented in revision transition.
                        delete(merged[ident])
                    elif ss.is_suppress or ss.is_update:
                        if old.is_live:
                            merged[ident] = ss
                        else:
                            # Only live ss's can be
                            # suppressed. Additionally, suppressed
                            # etc. ss's cannot be updated. We may want
                            # to change this so we can add messages
                            # etc. But it seems to me that the rebuild
                            # is not the time for doing this.
                            error()
                    else:
                        error() # Unknown operation
                else:
                    if ss.is_add:
                        merged[ident] = ss
                    elif ss.is_delete || ss.is_suppress || ss.is_update:
                        error() # Can't delete, move, suppress, or
                                # update non-existent ss
                    else:
                        error() # Unknown operation
            merge_ss_pdi.AddShortSequence(merged)
            # A real implementation will probably accumulate many runs
            # before actually calling the database write

    def Done:
        IndividuationSNPMap<SubSNP>.DisallowWriters(merged_ss_pdi.handle())

        IndividuationSNPMap<SubSNP>.Delete(new_ss_pdi.handle())
        IndividuationSNPMap<SubSNP>.Delete(init_ss_pdi.handle())

class RemapOperation:
    
    def Setup(ads, snp_type, src_snp_handle, blank_snp_handle):
        ads = ads
        IndividuationSNPMap<snp_type>.AllowWriters(blank_snp_handle)
        IndividuationSNPMap<snp_type>.DisallowWriters(src_snp_handle)
        source_pdi = IndividuationSNPMap<snp_type>(src_snp_handle)
        dest_pdi = IndividuationSNPMapMutable<snp_type>(blank_snp_handle)

    def Process(individuation_generator):
        for individuation in individuation_generator:
            # SNPs remap normally whether they are live or not. If
            # there is a record, it gets remapped. The same for RSes
            # -- they are remapped whether they are supported,
            # unsupported, or proxy
            source = source_pdi.At(individuation)
            if snp_type == SubSNP:
                dest = [ads.RemapSettingWasMoved(ss) for ss in source]
            elif snp_type = RefSNP:
                dest = [ads.RemapSettingWasMovedAndOldPtlp(rs) for rs in source]
            else:  #Unknown SNP type
                error()
            dest_pdi.AddShortSequence(dest)
            # A real implementation will probably accumulate many runs
            # before actually calling the database write

    def Done:
        IndividuationSNPMap<snp_type.DisallowWriters(source_pdi.handle)    
        IndividuationSNPMap<snp_type>.DisallowWriters(dest_pdi.handle)

class RSRebuildOperation:

    # Note that when it is after a remap, both the ss_handles and
    # rs_handle are assumed to be already remapped
    #
    # This means that they contain some bookeeping information about
    # their old PTLP and whether they were moved. For RefSNPs, this
    # might be implemented by just updating the PTLP in the envelope
    # and leaving the PTLP in the blob unchanged. Then if the two are
    # the same, it wasn't moved. If they are different, you know
    # from->to. This implementation avoids having to add fields to the
    # object or have two different types of RefSNP object, one with
    # bookkeeping and one without.
    def Setup(ss_handles, rs_handle, blank_rs_handle, rebuild_report_handle, 
              is_after_remap):
        IndividuationSNPMap<RefSNP>.DisallowWriters(rs_handle)
        rs_pdi = IndividuationSNPMap<RefSNP>(rs_handle)

        ss_pdis = []
        for h in ss_handles:
            IndividuationSNPMap<h.type>.DisallowWriters(h)
            ss_pdis.append(IndividuationSNPMap<h.type>(h))

        IndividuationSNPMap<RefSNP>.AllowWriters(blank_rs_handle)
        dest_pdi = IndividuationSNPMapMutable<RefSNP>(blank_rs_handle)

        report = RebuildReport(rebuild_report_handle)

    # Note that this individuation generator needs to return the union
    # of all individuations in all the source databases
    def Process(union_individuation_generator):
        for individuation in union_individuation_generator:
            all_rses = rs_pdi.At(individuation)
            # Separate the rs's based on where they are in the life-cycle
            [pubd_rses, unsup_rses, proxy_rses] = Categorize(all_rses)
            num_non_proxy = size(pubd_rses) + size(unsup_rses)
            parent_rs = [] #Empty if no parent
            new_proxy_rses = [] 
            if num_non_proxy > 1:
                if is_after_remap==only_remapping_done:
                    # Merge
                    [rs, parent_rs, new_proxy_rses, 
                     proxy_rses] = CreateSurvivorRS(
                        pubd_rses, unsupp_rses, proxy_rses)
                    event = merged_rs
                else:
                    # Merge should not happen in an un-remapped rebuild
                    error()
            elif is_empty(pubd_rses):
                if is_empty(unsupp_rses):
                    if is_empty(proxy_rses):
                        # Brand new RS
                        rs = NewEmptyRSWithNewRSID(individuation)
                        event = created_rs
                    else:
                        # If there are proxy rs's there must have been
                        # a merger in the past, so there should at
                        # least be an unsupported rs - so this should
                        # never happen
                        error()
                elif size(unsupp_rses) == 1:
                    # Resurrect rs (proxy rses don't matter
                    # here). This is really a special kind of update,
                    # so it won't be reported if there are still no
                    # subsnps - since the RS will be identical in that
                    # case. Thus we can call it a resurrection now.
                    rs = NewEmptyRS(individuation, unsupp_rses[0].Rsid())
                    parent_rs = unsupp_rses[0]
                    event = resurrect_rs
                else: #Can't happen since num_non_proxy <= 1
                    error() 
            elif size(pubd_rses) == 1:
                if is_empty(unsupp_rses):
                    # Update to existing RS
                    rs = NewEmptyRS(individuation, pubd_rses[0].Rsid())
                    parent_rs = pubd_rses[0]
                    event = updated_rs
                else: #Can't happen since num_non_proxy <= 1
                    error()

            # Add the support to the RS
            has_unpurged_support = False
            for pdi in ss_pdis:
                for ss in pdi.At(individuation):
                    if ss.is_live:
                        rs.Add(ss)
                        has_unpurged_support = True
                    elif ss.is_suppressed or ss.is_withdrawn:
                        has_unpurged_support = True
                        

            # Check for identical to see if can reuse uuid and if need
            # to report the "update"
            should_report = True
            if num_non_proxy == 1 and AreIdentical(rs, parent_rs):
                rs = parent_rs
                should_report = False

            if has_unpurged_support:
                dest_pdi.Add(StripBookeeping(rs)) # Again there would be
                                                  # chunking here in real
                                                  # life
                dest_pdi.Add(map(StripBookeeping,
                                 join(new_proxy_rses, proxy_rses)))
                                 # Adding empty list would do
                                 # nothing, so no problem doing
                                 # this when there was no merge
            if should_report:
                # Can report whether an RS became a proxy, was
                # resurrected, gained or lost subsnps (using the
                # parent), moved to a different location, changed PTLP
                # or was merged with another RS
                report.Add(event, has_unpurged_support, rs, 
                           parent_rs, new_proxy_rses, proxy_rses)

    def Done:
        IndividuationSNPMap<RefSNP>.DisallowWriters(dest_pdi.handle())

# Growing SubSNPs
while( (loc,ss_type) = GetSubmission()):
    new_data_pdi_handles[ss_type].ReceiveData(loc)

# A release before it is published
#
# m_ads - A handle to the ADS associated with this release
#
# m_growing_ss_handle - A handle to the PDI which contains all SS
#     (regardless of type) added during the growing phase. The PDI is
#     deleted and the handle invalidated after the Growing phase SS
#     are merged into the main SS
#
# m_phase - The phase (Initializing, Growing, Packaging) of this
#     release. A PrePublicRelease is not in the Public or Archived
#     phases by definition.
#
# m_release_number - The release number associated with this release
#
# m_creation_date - The date on which this release was created
#
# m_rs_handle - The handle to the PDI in which this release's RefSNPs
#     are stored.
#
# m_ss_handles - An associative array indexed by type of the handles
#     to the PDIs where this release's SubSNPs are stored
#
# m_triage - A handle to the triage process used by this release
#     (really the same through all releases but I thought this was
#     better than a global variable) 
#
# m_vault - A handle to the vault used by this release (also the same
#     for all releases)
#
class PrePublicRelease:
    # Constructor
    def PrePublicRelease(parent_release, ads):
        p = parent_release
        m_release_number = NextRelease(p.m_release_number)
        m_ads = ads

        # Remap SS
        m_ss_handles = []
        for ss_type in [h.type for h in p.m_ss_handles]:
            m_ss_handles[ss_type] = CreateEmptyPDI(ss_type)
            ro = RemapOperation(m_ads, ss_type, p.m_ss_handles[ss_type], 
                                m_ss_handles[ss_type])
            for generator in ro.source_pdi.IndividuationEnumerators():
                (parallel)
                ro.Process(generator)
        
        # Remap RS - can be done in parallel with SS remap
        m_rs_handle = CreateEmptyPDI(RefSNP)
        ro = RemapOperation(ads, RefSNP, p.m_rs_handle, 
                            m_rs_handle);
        for generator in ro.source_pdi.IndividuationEnumerators():
            (parallel)
            ro.Process(generator)

        m_creation_date = Now()
        m_phase = Initializing
        m_triage = p.m_triage
        m_vault = p.m_vault
        m_growing_ss_handle = CreateEmptyPDI(AnySS)

    def StopGrowingAndPromoteToPackaging():
        assert(m_phase == Growing)
        m_triage.StopSendingSubmissionsTo(self)
        m_phase = Packaging
        
    def CopySubSNPsIntoVault():
        pdi = IndividuationSNPMap<AnySS>(m_growing_ss_handle)
        for generator in pdi.IndividualEnumerators():
            (parallel)
            m_vault.AddAll(generator)

    def MergeNewSubSNPs():
        ss_pack_handles = []
        for ss_type in [h.type for h in m_ss_handles]:
            ss_pack_handles[ss_type] = CreateEmptyPDI( ss_type)
            mo = SSMergeOperation(
                new_data_pdi_handle[ss_type], m_ss_handles[ss_type], 
                ss_pack_handles[ss_type])
            for generator in UnionEnumerators(
                mo.merge_ss_pdi.IndividuationEnumerators(), 
                mo.new_ss_pdi.IndividuationEnumerators()): 

                (parallel)
                mo.Process(generator)
                pass

        ReplacePDIDeletingOld(m_ss_handles, ss_pack_handles)
        DeletePDI(m_growing_ss_handle)
        m_growing_ss_handle = InvalidPDIHandle()

    def ReplacePDIDeletingOld(member, new_handle):
        old = member
        member = new_handle
        if old.IsList():
            DeletePDIs(old)
        else:
            DeletePDI(old)

    # Rebuild the RefSNPs in this release
    #
    # Needed immediately after creation and after merging. After
    # creation, pass from_ADS_change and after merging, pass
    # from_new_submissions.
    #
    # Param: 
    #
    # change_type - Indicates the change that required the rebuild,
    #     either from_new_submissions (when a rebuild was required
    #     because new submissions were merged after the growth phase)
    #     or from_ADS_change (when SNPs are being copied to a new
    #     release with a different ADS)
    #
    # Returns a handle to a report object that aggregates all reports
    #     generated during the rebuilding (e.g. per-process reports
    #     generated by each worker-node in a gpipe graph)
    def RebuildRefSNPs(change_type):
        remap_type = no_remapping_done
        if change_type = from_ADS_changes: #other option is from_new_submissions
            remap_type = only_remapping_done

        rebuilt_handle = CreateEmptyPDI(m_rs_handle.type)
        report_handle = NewRebuildReport(m_release_number, change_type)
        rReb = RSRebuildOperation(m_ss_handles, m_rs_handle, 
                                  rebuilt_handle, report_handle,
                                  remap_type)
        for generator in UnionEnumerators(rReb.rs_pdi, rReb.ss_pdis):
            (parallel)
            rReb.Process(generator)
        aggregated_report = NewAggregatedReport(report_handle)
        report_handle.Delete() # Unaggregated object is not needed anymore

        ReplacePDIDeletingOld(m_rs_handle, rebuilt_handle)
        return aggregated_report;

    def CopyRefSNPsIntoVault():
        pdi = IndividuationSNPMap<RefSNP>(m_rs_handle)
        for generator in pdi.IndividualEnumerators():
            (parallel)
            m_vault.AddAll(generator)

    def PromoteToGrowing():
        assert(m_phase == Initializing)
        m_phase = Growing
        m_triage.StartSendingSubmissionsTo(self)

def TransitionToNextRel(growing_release):
    growing_release.StopGrowingAndPromoteToPackaging()
    packaging = growing_release
    packaging.CopySubSNPsIntoVault()
    packaging.MergeNewSubSNPs()
    report = packaging.RebuildRefSNPs(from_new_submissions)
    if report.IsBad():
        StopBuild()
    ParallelExec(TransitionToNextRelPartB(packaging), Annotate(packaging))

def TransitionToNextRelPartB(packaging_release):
    packaging_release.CopyRefSNPsIntoVault()
    init = new PrePublicRelease(packaging_release, GetMostRecentADS())
    report = init.RebuildRefSNPs(from_ADS_change)
    if report.IsBad():
        StopBuild()
    init.PromoteToGrowing()
