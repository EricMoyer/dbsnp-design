/*  $Id: Revision.hpp 450936 2014-10-31 17:13:35Z moyered $
 * ===========================================================================
 *
 *                            PUBLIC DOMAIN NOTICE
 *               National Center for Biotechnology Information
 *
 *  This software/database is a "United States Government Work" under the
 *  terms of the United States Copyright Act.  It was written as part of
 *  the author's official duties as a United States Government employee and
 *  thus cannot be copyrighted.  This software/database is freely available
 *  to the public for use. The National Library of Medicine and the U.S.
 *  Government have not placed any restriction on its use or reproduction.
 *
 *  Although all reasonable efforts have been taken to ensure the accuracy
 *  and reliability of the software and data, the NLM and the U.S.
 *  Government do not and cannot warrant the performance or results that
 *  may be obtained by using this software or data. The NLM and the U.S.
 *  Government disclaim all warranties, express or implied, including
 *  warranties of performance, merchantability or fitness for any particular
 *  purpose.
 *
 *  Please cite the author in any work or product based on this material.
 *
 * ===========================================================================
 *
 */

/// \file
///
/// \author Eric Moyer
///
/// \brief descibes a Revision object
///
/// A revision object represents one revision of dbSNP. There is no
/// single revision class. There is a different class with different
/// capabilities representing the different phases of the life-cycle
/// of a revision.
///
/// Each revision is connected to a RevisionDatabase that persists
/// most of its state and links it to the other various databases that
/// hold its component SNPs (if any). A very small portion of the
/// state of each revision, its life-cycle stage is stored in the
/// global dbSNP database and can send it messages to synchronize the
/// global picture of which revision is in which phase of its
/// life-cycle.

#include "DbSnp.hpp"
#include "Utility.hpp"
#include "../mini_pdi/Jens.hpp" //Definitely against coding style

#ifndef INCLUDED_REVISION_HPP_EDM
#define INCLUDED_REVISION_HPP_EDM

BEGIN_NCBI_SCOPE
BEGIN_SCOPE(NDbSnp)

/// The NRevision namespace contains all the Revision life-cycle
/// phases and their related functions
BEGIN_SCOPE(NRevision);
class Initializing{};

/// Represents a Growing revision. There can only be one growing
/// revision at any one time - though there may be multiple objects in
/// multiple places that refer to it.
///
/// We need to ensure that there is at most one Growing revision
/// object in existence when the data freeze happens and Growing
/// becomes Packaging
class Growing
{
    using namespace std;
public:
    /// \brief Add SubSNPs that have passed through triage - batches are
    /// potentially on disk and large
    ///
    /// The triage process will get the current growing revision from
    /// the global DbSnp object and then use this method to add its
    /// new snps.
    ///
    /// These snps should be added to the <ask brad because I think we changed things from when my SS life-cycle was written>
    ///
    /// \param snps The snps to add
    ///
    /// \note I doubt this will be a bottleneck, but if it is, we can
    ///     change to a TInputJenP<TInputJenP<CRef<CBatch>>> and
    ///     distribute the work from the different Jens
    void AddBatches(TInputJenP<CRef<CBatch> > > batches);
};
class Packaging{};
class Public{};
class Archived{};

END_SCOPE(NRevision)

END_SCOPE(NDbSnp)
END_NCBI_SCOPE

#endif //INCLUDED_REVISION_HPP_EDM
